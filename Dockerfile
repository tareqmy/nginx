FROM nginx:latest
MAINTAINER tareqmohammadyousuf "tmyousuf@vantage.com"

RUN apt-get -y update && \
    apt-get -y install curl cron vim unzip wget && \
    mkdir -p /app && rm -fr /usr/share/nginx/html && ln -s /app /usr/share/nginx/html && \
    echo "America/New_York" > /etc/timezone && dpkg-reconfigure -f noninteractive tzdata

COPY configs/maxcore.conf /etc/nginx/conf.d/
COPY configs/keys/* /etc/nginx/conf.d/keys/

COPY *.sh /opt/
RUN chmod +x /opt/*

ENTRYPOINT ["/opt/entrypoint.sh"]

WORKDIR /usr/share/nginx/html

EXPOSE 443
