#!/bin/bash
set -e

cd /etc/nginx/conf.d/
sed -i "s/servername/$DNS_NAME/" maxcore.conf

service nginx start

/opt/updater.sh
