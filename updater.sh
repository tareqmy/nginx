#!/usr/bin/env bash


    lastmd5='none'

if [ -z "$ZIP_REFRESH" ]; then 

        # Download the latest ZIP file.
        wget -q -P /opt -N $ZIP_URL

        file=`find /opt -name *.zip`
        filemd5=`md5sum $file | cut -d " " -f1`


        # deploy the app
        echo "/-/ Unpackaging the application."
        unzip -o $file -d /usr/share/nginx/html
        echo "/-/ Application has been unpackaged."
       

        # Create the log file to be able to run tail
        touch /var/log/cron.log        

        # install cron job 1
         if [ -n "$CRON_JOB1" ]; then
        echo "$CRON_JOB1" > crontab-1
         cp crontab-1 /etc/cron.d/
         fi
        # install cron job 2
         if [ -n "$CRON_JOB2" ]; then
        echo "$CRON_JOB2" > crontab-2
         cp crontab-2 /etc/cron.d/
         fi

        while true; do
          sleep 60
        done
else


        # Create the log file to be able to run tail
        touch /var/log/cron.log

        # install cron job 1
         if [ -n "$CRON_JOB1" ]; then
        echo "$CRON_JOB1" > crontab-1
         cp crontab-1 /etc/cron.d/
         fi
        # install cron job 2
         if [ -n "$CRON_JOB2" ]; then
        echo "$CRON_JOB2" > crontab-2
         cp crontab-2 /etc/cron.d/
         fi

        #check for app updates

    while true; do
        # Download the latest ZIP file.
        wget -q -P /opt -N $ZIP_URL

        file=`find /opt -name *.zip`
        filemd5=`md5sum $file | cut -d " " -f1`

        # If ZIP has changed, restart the application.
        if [ $filemd5 != $lastmd5 ]
        then
            # deploy the app
            echo "/-/ Unpackaging the application."
            unzip -o $file -d /usr/share/nginx/html
            echo "/-/ Application has been unpackaged."
        fi

        lastmd5=$filemd5;
        sleep 15
        echo "/-/ Checking for updated application package...."
    done

fi
